FROM node:13.10.1-buster

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y nodejs

COPY views/ /app/views/

COPY sandbox/ /app/sandbox/

COPY server.js /app/server.js

COPY *.json /app/

COPY run_sonar_scanner.sh /app/run_sonar_scanner.sh

RUN chmod a+x /app/run_sonar_scanner.sh

WORKDIR /app

RUN mkdir sonar-scanner

RUN groupadd super-group -g 666

RUN useradd -m super-admin --uid=666 -s /bin/bash --gid=666

RUN export VM_UID=666 && export VM_GID=666

RUN npm install

ENTRYPOINT ["node", "server.js"]