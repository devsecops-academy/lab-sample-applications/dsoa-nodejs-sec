#! /bin/bash

print_warn() {
    message=$1
    echo "[WARN]: $message"
}

print_error() {
    message=$1
    echo "[ERROR]: $message"
}

download_sonar_scanner() {
    sonar_version=$1
    # Download the SonarQube image
    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/$sonar_version -O sonar-scanner/scanner.zip
}

install_deps_and_run_scanner() {
    if [ "$OSTYPE" == "linux-gnu" ]; then
        if [[ ! -f "./sonar-scanner/sonar-scanner-4.2.0.1873-linux/bin/sonar-scanner" ]]; then
            apt-get update 
            apt-get upgrade -y
            apt-get install default-jre unzip -y
            download_sonar_scanner sonar-scanner-cli-4.2.0.1873-linux.zip
            unzip sonar-scanner/scanner.zip -d ./sonar-scanner/
        fi
        eval ./sonar-scanner/sonar-scanner-4.2.0.1873-linux/bin/sonar-scanner $SCANNER_OPT;
    elif [ "$OSTYPE" == "darwin"* ]; then
        if [ ! -f "./sonar-scanner/sonar-scanner-4.2.0.1873-macosx/bin/sonar-scanner" ]; then
            download_sonar_scanner sonar-scanner-cli-4.2.0.1873-macosx.zip
            unzip sonar-scanner/scanner.zip -d ./sonar-scanner/
        fi
        eval ./sonar-scanner/sonar-scanner-4.2.0.1873-macosx/bin/sonar-scanner $SCANNER_OPT;
    fi
}

if [ -z $SONAR_HOST ]; then
    SONAR_HOST=http://localhost:9000;
    print_warn "Using default value for SONAR_HOST";
fi

if [ -z $SONAR_PROJECT_KEY ]; then
    SONAR_PROJECT_KEY=nodesec;
    print_warn "Using default value for SONAR_PROJECT_KEY";
fi

if [ -z $SONAR_LOGIN ]; then
    print_error "No value set for SONAR_LOGIN";
fi

if [ -z $SONAR_SRC_DIR ]; then
    SONAR_SRC_DIR=sandbox/;
    print_warn "Using default value for SONAR_SRC_DIR";
fi

export SONAR_SCANNER_OPTS="-Xmx2048m";
echo "Using $SONAR_HOST as sonar.host.url";
echo "Using $SONAR_PROJECT_KEY as sonar.projectKey";
echo "Using $SONAR_LOGIN as sonar.login";

SCANNER_OPT="-Dsonar.host.url=$SONAR_HOST -Dsonar.projectBaseDir=$SONAR_SRC_DIR -Dsonar.login=$SONAR_LOGIN  -Dsonar.projectKey=$SONAR_PROJECT_KEY -Dsonar.sources=. -Dsonar.exclusions=backup**,challenge** -X"
install_deps_and_run_scanner
