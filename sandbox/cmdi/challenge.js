const vm = require("vm");
const fs = require("fs");

const child_process = require("child_process");
const utils = require("util");

// Configurations
const contextConfig = {
    name: "FaultVM",
    codeGeneration: {
        strings: false,
        wasm: false,
    },
};

const sandbox = {
    require: function(moduleName) {
        return {
            child_process: {
                exec(command, callbackFn) {
                    child_process.exec(command, {
                        uid: process.env["VM_UID"],
                        gid: process.env["VM_GID"],
                    }, callbackFn);
                },
                execFile: child_process.execFile,
            },
            util: {
                promisify: utils.promisify,
            }
        }[moduleName];
    },
    res: {
        send: (data) => {
            results = data;
            return;
        },
    },
    results: [],
    log: console.log,
};

module.exports = {
    executeSandboxedScript: async (parameter) => {
        const context = vm.createContext(sandbox, contextConfig);

        const rawScript = fs.readFileSync("sandbox/cmdi/index.js").toString();
        const script = new vm.Script(rawScript);

        script.runInContext(context);

        const runtimeScript = new vm.Script(`
        async function main() {
            const req = {
                body: {
                    address: "${parameter}",
                },
            };

            results = await ping(req, res);
        }

        main();
        `, {
            filename: "runtime.js",
        });

        await runtimeScript.runInContext(context);

        console.log(context);

        return context.results;
    }
}
