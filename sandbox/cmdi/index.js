var exec = require("child_process").exec;

function ping(req, res) {
    return new Promise((resolve, reject) => {
        exec('ping -c 2 ' + req.body.address, function (err, stdout, stderr) {
            var output = stdout + stderr;
            res.send(output);
            
            return resolve(output);
        });
    });
}