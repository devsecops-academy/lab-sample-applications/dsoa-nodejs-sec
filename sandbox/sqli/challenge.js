const vm = require("vm");
const fs = require("fs");

const child_process = require("child_process");
const utils = require("util");

const { Parser } = require("node-sql-parser");

const database = require("./database");
const processor = require("./queryProcessor");

// Configurations
const contextConfig = {
    name: "FaultVM",
    codeGeneration: {
        strings: false,
        wasm: false,
    },
};

const sandbox = {
    database: {
        async rawQuery(rawSQL) {
            const parser = new Parser();

            if (rawSQL) {
                const AST = parser.astify(rawSQL);

                return processor.executeSQL(AST, database);
            } else {
                console.log("Error while receiving remote query.", rawSQL);
            }
        },
    },
    log: (data) => console.log(data),
    data: [],
};

module.exports = {
    executeSandboxedScript: async (parameter) => {
        const context = vm.createContext(sandbox, contextConfig);

        const rawScript = fs.readFileSync("./index.js").toString();
        const script = new vm.Script(rawScript);

        script.runInContext(context);

        const runtimeScript = new vm.Script(`
        async function main() {
            const req = {
                body: {
                    address: "${parameter}",
                },
            };

            results = await ping(req, res);
        }

        main();
        `, {
            filename: "runtime.js",
        });

        await runtimeScript.runInContext(context);

        console.log(context);

        return context.results;
    }
}
