module.exports = {
    tables: {
        products: [
            {
                name: "ThreadRipper 3990X",
                manufacturer: "AMD",
                price: "$5000.00",
            },
            {
                name: "Ryzen 7",
                manufacturer: "AMD",
                price: "$2000.00",
            },
            {
                name: "Xeon Platinum",
                manufacturer: "Intel",
                price: "$10,000.00",
            }
        ],
        users: [
            {
                name: "John Doe",
                password: "81dc9bdb52d04dc20036dbd8313ed055",
            },
            {
                name: "Super Admin",
                password: "482c811da5d5b4bc6d497ffa98491e38",
            }
        ],
    },
};