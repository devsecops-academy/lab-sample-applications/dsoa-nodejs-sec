function reportError(msg) {
    return {
        error: msg,
    }
}

// handleBinaryExpressions should resolve which data have to come
// from a given table dataset.
function handleBinaryExpressions(tableSource, rootTableName, dataSource, AST) {
    let preliminaryResults = [];
    const tokenType = AST["type"];

    if (tokenType != "binary_expr") {
        return reportError(`Invalid token, expected BINARY, received ${tokenType}`);
    }

    const leftHand = AST["left"];
    const rightHand = AST["right"];

    const operator = AST["operator"];

    if (leftHand && rightHand) {
        for (const data of tableSource) {
            let leftHandValue = "";
            let rightHandValue = "";


            if (leftHand.table && (leftHand.table !== rootTableName || leftHand.type !== "column_ref")) {
                leftHandValue = [];

                for (const foreignData of dataSource[leftHand.table]) {
                    leftHandValue.push(foreignData[leftHand.column]);
                }
            } else if (leftHand.type === "select") {
                leftHandValue = processSelect(dataSource, leftHand);
            } else if (leftHand.type === "binary_expr") {
                leftHandValue = handleBinaryExpressions(tableSource, rootTableName, dataSource, leftHand);
            } else {
                leftHandValue = leftHand.type === "column_ref" 
                                ? data[leftHand.column] : leftHand.value;
            }

            if (rightHand.table && rightHand.table !== rootTableName) {
                rightHandValue = [];

                for (const foreignData of dataSource[rightHand.table]) {
                    rightHandValue.push(foreignData[rightHand.column]);
                }
            } else if (rightHand.type === "select") {
                rightHandValue = processSelect(dataSource, rightHand);
            } else if (leftHand.type === "binary_expr") {
                rightHandValue = handleBinaryExpressions(tableSource, rootTableName, dataSource, rightHand);
            } else {
                rightHandValue = rightHand.type === "column_ref" 
                                ? data[rightHand.column] : rightHand.value;
            }
    
            if (operator === "OR") {
                if (rightHandValue || leftHandValue) {
                    preliminaryResults.push(data);
                }
            }

            if (operator === "AND") {
                if (rightHandValue && leftHandValue) {
                    preliminaryResults.push(data);
                }
            }

            if (operator === "=") {
                if (rightHandValue == leftHandValue) {
                    preliminaryResults.push(data);
                }
            }

            if (operator === "!=") {
                if (rightHandValue != leftHandValue) {
                    preliminaryResults.push(data);
                }
            }
        }

        return preliminaryResults;
    } else {
        return reportError(`Invalid syntax, BINARY EXPRESSION should have left and right hand.`)
    }
}

function processSelect(dataSource, AST) {
    const tokenType = AST["type"];
    
    if (tokenType !== "select") {
        return reportError(`Invalid token, expected SELECT, received ${tokenType}`);
    }
    
    // Now that we are sure that we have a SELECT token, we search for the given structures
    // that must be searched.
    const selectedTable = AST["from"][0].table;
    const conditionToken = AST["where"];
    const selectedColumns = AST["columns"];

    const otherOperators = {
        groupBy: AST["groupby"],
        orderBy: AST["orderby"],
        limit: AST["limit"],
    }
    
    if (!selectedTable) {
        return reportError("An error ocurred when trying to resolve query.");
    }
    
    const tableData = dataSource.tables[selectedTable];

    if (selectedColumns !== "*") {
        // Handle projections
    }

    if (!conditionToken) {
        return tableData;
    }

    return handleBinaryExpressions(tableData, selectedTable, dataSource, conditionToken);
}

module.exports = {
    executeSQL(AST, dataSource) {
        if (!Object.keys(dataSource).includes("tables")) {
            return reportError("Invalid data source");
        }
        
        const results = processSelect(dataSource, AST);

        return results;
    }
}