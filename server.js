const polka = require("polka");
const fetch = require("node-fetch");
const http = require("http");

const fs = require("fs");
const childProcess = require("child_process");

const cmdiSandboxRuntime = require("./sandbox/cmdi/challenge.js");
// const sqliSandboxRuntime = require("sandbox/sqli/challenge.js");

const cmdiBackupData = fs.readFileSync("./sandbox/cmdi/backup.js").toString();
// const sqliBackupData = fs.readFileSync("sandbox/sqli/backup.js").toString();

const SONAR_API_PREFIX = process.env["SONAR_API_PREFIX"] || "http://localhost:9000";

console.log(`Connecting on SonarQube API on -> ${SONAR_API_PREFIX}`);

const ACTIONS = {
    "EXPLOIT": async (parameters) => {
        const { itemName } = parameters;
        const data = await cmdiSandboxRuntime.executeSandboxedScript(itemName);

        return data;
    },
    "RESET": () => {
        fs.writeFileSync("sandbox/cmdi/index.js", cmdiBackupData);

        return {
            result: "OK",
        };
    },
}

polka()
    .get("/sillabus", (_, res) => {
        res.end(fs.readFileSync("views/index.html"));
    })
    .get("/challenge", (_, res) => {
        res.end(fs.readFileSync("views/course.html"))
    })
    .post("/execute", function(req, res) {
        console.log("Received execution command");
        let reqBuffer = "";
        
        req.on("data", (chunk) => reqBuffer += chunk);

        req.on("end", async () => {
            const data = JSON.parse(reqBuffer);

            if (data && data.action) {
                if (Object.keys(ACTIONS).includes(data.action)) {
                    const result = await ACTIONS[data.action](data.parameters);

                    res.end(JSON.stringify(result));
                } else {
                    res.end(JSON.stringify({
                        error: "HOST: Invalid ACTION to execute",
                    }));
                }
            } else {
                res.end(JSON.stringify({
                    error: "HOST: Invalid payload to execute",
                }));
            }
        });
    })
    .get("/script", (_, res) => {
        const scriptData = fs.readFileSync("sandbox/cmdi/index.js").toString();

        res.end(JSON.stringify({
            scriptData,
        }));
    })
    .get("/execSonar", async (_, res) => {
        let analysisToken = "";

        if (!fs.existsSync("token.json")) {
            const projectParams = new URLSearchParams();
            const loginParams = new URLSearchParams();
            const analysisTokenParams = new URLSearchParams();

            const userAgent = "NodeSec tool 1.0.0";

            projectParams.append("name", "Node Security");
            projectParams.append("project", "nodesec");

            loginParams.append("login", "admin");
            loginParams.append("password", "admin");

            // Try to create the project to upload this analysis
            // If the SonarQube already have the project the response
            // will be an error, but that okay, both ways we know
            // that after this request there is a nodesec project.
            await fetch(`${SONAR_API_PREFIX}/api/projects/create`, {
                method: "POST",
                body: projectParams,
                headers: {
                    "User-Agent": userAgent,
                }
            });

            // Generate token for analysis
            const loginResponse = await fetch(`${SONAR_API_PREFIX}/api/authentication/login`, {
                method: "POST",
                body: loginParams,
                headers: {
                    "User-Agent": userAgent,
                }
            });

            analysisTokenParams.append("name", "NodeSecurity SAST pipeline");

            const cookies = loginResponse.headers.raw()['set-cookie'].map(cookie => {
                return cookie.split(";")[0];
            });

            const analysisTokenResponse = await fetch(`${SONAR_API_PREFIX}/api/user_tokens/generate`, {
                method: "POST",
                body: analysisTokenParams,
                headers: {
                    cookie: cookies,
                    "Content-type": "application/x-www-form-urlencoded",
                    "User-Agent": userAgent,
                    "Authorization": `Basic ${Buffer.from("admin:admin").toString('base64')}`
                }
            });

            const analysisTokenRawData = await analysisTokenResponse.text();
            const analysisTokenData = JSON.parse(analysisTokenRawData);

            console.log(analysisTokenRawData);

            if (analysisTokenRawData) {
                fs.writeFileSync("token.json", analysisTokenRawData, {
                    mode: 0600,
                });

                const tokenData = fs.readFileSync("token.json");

                if (JSON.parse(tokenData).token !== analysisTokenData.token) {
                    res.end(JSON.stringify({
                        error: "Failed to persist SonarQube API token, better restart the container."
                    }));
                }
            }

            analysisToken = analysisTokenData.token;
        } else {
            const rawTokenData = fs.readFileSync("token.json");
            const tokenData = JSON.parse(rawTokenData);

            analysisToken = tokenData.token;
        }

        console.log(analysisToken);

        const isRunnerScriptHere = fs.statSync("./run_sonar_scanner.sh");

        if (isRunnerScriptHere.isFile()) {
            const scanner = childProcess.spawn("bash", ["run_sonar_scanner.sh"], {
                maxBuffer: 1000000000, // I hope it is 1 GB
                env: {
                    "SONAR_LOGIN": analysisToken,
                    "SONAR_HOST": SONAR_API_PREFIX,
                    "SONAR_SRC_DIR": "sandbox/cmdi"
                }
            });

            scanner.stdout.on("data", (data) => console.log(data.toString()));
            scanner.stderr.on("data", (data) => console.log(data.toString()));

            res.end(JSON.stringify({
                data: "Executing SonarQube Scanner",
            }));
        } else {
            res.send(JSON.stringify({
                error: 'Could not find run_sonar_scanner.sh',
            }));
        }
    })
    .post("/applyPatch", (req, res) => {
        console.log("Received patch script command");
        let reqBuffer = "";
        
        req.on("data", (chunk) => reqBuffer += chunk);

        req.on("end", () => {
            const data = JSON.parse(reqBuffer);

            if (data && data.patch) {
                fs.writeFileSync("sandbox/cmdi/index.js", data.patch);
            }
        });

        res.end(JSON.stringify({ result: "OK" }));
    })
    .listen(3000, () => {
        console.log("Server listening on port 3000");
    });